import os, sys
from setuptools import setup, find_packages

version = '0.0.1'

setup(
    name='aiorequests',
    version=version,
    author='Luke Hodkinson',
    author_email='luke.hodkinson@structrs.com',
    maintainer='Luke Hodkinson',
    maintainer_email='luke.hodkinson@structrs.com',
    url='https://gitlab.com/structrs/aiorequests.git',
    description='Syntactic sugar around aiohttp.',
    long_description=open(os.path.join(os.path.dirname(__file__), 'README.md')).read(),
    classifiers = [
        'Development Status :: 3 - Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: BSD License',
        'Natural Language :: English',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python :: 2',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.3',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5'
    ],
    license='BSD',
    packages=find_packages(),
    include_package_data=True,
    package_data={'': ['*.txt', '*.js', '*.html', '*.*']},
    install_requires=[
        'aiohttp'
    ],

    # TODO: Remove
    zip_safe=False
)
