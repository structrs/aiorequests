import sys

import ujson
from aiohttp import ClientSession
from asyncio_extras.contextmanager import async_contextmanager

__all__ = (
    'get',
    'post',
    'patch',
    'delete'
)


@async_contextmanager
async def call(method, url, payload={}, json=True, form_data=False,
               headers=None, bearer=None, files=None):

    # Determine the options to pass to ClientSession.
    opts = {}

    # Where do we put the payload? Depends on the method and the
    # content-type.
    if method == 'get' and payload:
        opts['params'] = payload
    elif payload:
        if json and not form_data:
            opts['json'] = payload
        else:
            opts['data'] = payload

    # If files are included, add them.
    if files is not None:
        opts['files'] = files

    # Add any additional headers.
    hdrs = {}
    if json and not form_data:
        hdrs['Content-Type'] = 'application/json'
    if bearer:
        hdrs['Authorization'] = f'Bearer {bearer}'
    if headers:
        hdrs.update(headers)
    if hdrs:
        opts['headers'] = hdrs

    # Open the session and make the request.
    async with ClientSession(json_serialize=ujson.dumps) as client:
        async with getattr(client, method)(url, **opts) as r:
            yield r


@async_contextmanager
async def get(*args, **kwargs):
    async with call('get', *args, **kwargs) as r:
        yield r


@async_contextmanager
async def post(*args, **kwargs):
    async with call('post', *args, **kwargs) as r:
        yield r


@async_contextmanager
async def patch(*args, **kwargs):
    async with call('patch', *args, **kwargs) as r:
        yield r


@async_contextmanager
async def delete(url, *args, **kwargs):
    async with call('delete', *args, **kwargs) as r:
        yield r
