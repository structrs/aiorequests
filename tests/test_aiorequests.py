from asynctest import TestCase

from aiorequests import get, post, patch, delete


class GetTestCase(TestCase):
    async def test_valid_url(self):
        async with get('http://www.google.com/', json=False) as r:
            c = await r.text()
            self.assertGreater(len(c), 0)

    async def test_invalid_url(self):
        with self.assertRaises(ValueError):
            async with get('www.google.com', json=False) as r:
                pass

    async def test_json(self):
        async with get('https://jsonplaceholder.typicode.com/users') as r:
            c = await r.json()
            self.assertEqual(len(c), 10)

    def test_bearer(self):
        # TODO
        pass
