# AsyncIO Requests

Designed to be dead simple, but that also means no bells and
whistles. Really just syntactic sugar around `aiohttp`.

## Quickstart

```python
import aiorequests

async def go():
    url = 'https://jsonplaceholder.typicode.com/users'
    async with aiorequests.get(url, json=True) as response:
        data = await response.json()

loop = asyncio.new_event_loop()
loop.run_until_complete(go())
loop.close()
```
